#!/bin/bash

### grapemaker.sh -- Making fake disk dumps of Vinograd for you.

# This script is intended to help with producing virtual disk images
# with contents based on a directory.
#
# Synopsis:
#   ./grapemaker.sh <disk-name> <disk-size-in-mb> <partitons-contents-dir>
#
# Usage example:
#   ./grapemaker.sh grape 100 partitions
#
#   This command creates a disk 100MB in size and fills it with data.

# Copyright (C) 2018 Artyom V. Poptsov <poptsov.artyom@gmail.com>
#
# grapemaker is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# grapemaker is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with grapemaker.  If not, see <http://www.gnu.org/licenses/>.

###

# Create an empty disk.
#
# Synopsis:
#   mk_empty_disk name size
mk_empty_disk() {
    local name="$1"
    local size="$2"
    if [ ! -e "$name" ]; then
        dd if=/dev/zero of="$name" bs=1M count="$size"
    else
        echo "ERROR: File '$name' exists"
        exit 1
    fi
}

# Make a GPT partition table on a disk DISK_NAME.
#
# Synopsis:
#   mk_partition_table disk_name
mk_partition_table() {
    local disk_name="$1"
    if [ -z "$disk_name" -o ! -f "$disk_name" ]; then
        echo "ERROR: Wrong disk: $disk_name"
        exit 1
    fi
    echo -e "mk_partition_table: Initializing disk '$disk_name' ...\n"
    (
        echo o                   # create a new DOS partition table
        echo w                   # write changes to the disk
    ) | fdisk "$disk_name"
    echo -e "\nmk_partition_table: Initializing disk '$disk_name' ... done"
}

# Make a new partition on a disk DISK_NAME.
#
# Synopsis:
#   mk_partition disk_name partition_number partition_size
mk_partition() {
    local disk_name="$1"
    local partition_number="$2"
    local partition_size="$3"

    if [ -z "$disk_name" -o ! -f "$disk_name" ]; then
        echo "ERROR: Wrong disk $disk_name"
        exit 1
    fi

    (
        echo n                   # add a new partition
        echo p                   # primary partition
        echo "$partition_number" # partition number
        echo                     # first sector (acccept default: 1)
        echo "+${partition_size}M"   # partition size
        echo w                   # write changes to the disk
    ) | fdisk "$disk_name"
}

# Remove a partition PARTITION_NUMBER from a disk DISK_NAME.
#
# Synopsis:
#   rm_partition disk_name partition_number
rm_partition() {
    local disk_name="$1"
    local partition_number="$2"

    if [ -z "$disk_name" -o ! -f "$disk_name" ]; then
        echo "ERROR: Wrong disk $disk_name"
        exit 1
    fi

    (
        echo d
        echo "$partition_number" # partition number
        echo
        echo w
    ) | fdisk "$disk_name"
}

# Initialize a new disk.
#
# XXX: This procedure operates only on primary partitions.
#
# Syntax:
#   disk_init disk_name disk_size
disk_init() {
    local disk_name="$1"
    local disk_size="$2"         # megabytes

    # Size of the (disk - 1) / 4 - to count 1M of alignment
    local partition_size=$(( (disk_size - 1) / 4))

    echo $partition_size

    mk_partition_table "$disk_name"

    echo -e "disk_init: creating partitions ...\n"
    for partnum in $(seq 1 4); do
        mk_partition "$disk_name" "$partnum" "$partition_size"
    done
    echo -e "\ndisk_init: creating partitions ... done"
}

# Return partition size in bytes.
#
# Synopsis:
#   get_partition_size disk_name
get_partition_size() {
    local disk_name="$1"
    local partition_sectors=$(\
            fdisk -l "$disk_name" | tail -4 \
                | awk '{print $4}' \
                | head -1)
    local partition_size=$((partition_sectors * 512))
    echo "$partition_size"
}

# Get disk offsets in bytes
#
# Synopsis:
#   get_disk_offsets disk_name
get_disk_offsets() {
    local disk_name="$1"
    fdisk -l "$disk_name" | tail -4 \
        | awk '{print $2}' \
        | while read offset; do
        echo $((offset * 512))
    done
}

# Make a loop device for the given PARTITION_NUMBER from a disk
# DISK_NAME.  Return a loop device path.  A caller _must_ delete the
# loop device.
#
# Synopsis:
#   make_loop_device disk_name partition_number
make_loop_device() {
    local disk_name="$1"
    local partition_number="$2"
    local offset=$(get_disk_offsets "$disk_name" | sed -n "${partition_number}p")
    local partition_size=$(get_partition_size "$disk_name")
    local loop_device=$(losetup -f)
    losetup -o $offset --sizelimit "$partition_size" \
            "$loop_device" "$disk_name"
    if [ $? -gt 0 ]; then
        echo "ERROR: Could not create a loop device: $loop_device"
        exit 1
    fi

    echo "$loop_device"
}

# Mount a partition PARTITION_NUMBER from a disk DISK_NAME to a
# MOUNT_POINT.  Return a loop device path.  A caller _must_ delete the
# loop device.
#
# Synopsis:
#   mount_partition disk_name partition_number mount_point
mount_partition() {
    local disk_name="$1"
    local partition_number="$2"
    local mount_point="$3"
    local loop_device=$(make_loop_device "$disk_name" "$partition_number")
    mount "$loop_device" "$mount_point"
    echo "$loop_device"
}

# Mount an encrypted partition PARTITION_NUMBER from a DISK_NAME to a
# MOUNT_POINT.
#
# Synopsis:
#   mount_encrypted_partition disk_name partition_number mount_point
mount_encrypted_partition() {
    local disk_name
    local partition_number
    local mount_point
    local loop_device
    local password pim
    # Parse CLI options
    local temp=$(getopt -o p: --long password:,pim: -- "$@")
    eval set -- "$temp"

    while true; do
        case "$1" in
            --password | -p)
                password="$2"
                shift 2
                ;;
            --pim)
                pim="$2"
                shift 2
                ;;
            *)
                break
                ;;
        esac
    done
    disk_name="$2"
    partition_number="$3"
    mount_point="$4"
    loop_device=$(make_loop_device "$disk_name" "$partition_number")

    veracrypt \
        --text \
        --non-interactive \
        --password="$password" \
        --pim="$pim" \
        "$loop_device" "$mount_point"
}

# Dismount an encrypted partition.  The caller must not delete the
# related loop device because veracrypt deletes it on its own.
#
# Synopsis:
#   dismount_encrypted_partition mount_point
dismount_encrypted_partition() {
    local mount_point="$1"
    veracrypt \
        --text \
        --non-interactive \
        --dismount \
        "$mount_point"
}

###

# Format partitions PNUM_1 PNUM_2 ... PNUM_N on a disk to ext4.
#
# Synopsis:
#   disk_format_partitions disk_name pnum_1 pnum_2 ... pnum_n
disk_format_partitions() {
    local disk_name="$1"
    local loop_device

    shift 1                     # to skip disk_name

    for pnum in $@; do
        echo "Formatting partition $pnum ..."
        loop_device=$(make_loop_device "$disk_name" "$pnum")
        echo "  loop device: $loop_device"
        mkfs.ext4 "$loop_device"
        if [ $? -gt 0 ]; then
            echo "ERROR: Could not make a filesystem on '$loop_device'"
            exit 1
        fi
        losetup -d "$loop_device"
        echo "Formatting partition $pnum ... done"
    done
}

# Mount each partition from an image DISK_NAME in order and copy data
# to it from PARTITIONS_DIR.
#
# Synopsis:
#   copy_data disk_name partitions_dir mount_point
copy_data() {
    local disk_name="$1"
    local partitions_dir="$2"
    local mount_point="$3"
    local pnum=1
    local loop_device
    local source_dir
    local partition_size=$(get_partition_size "$disk_name")

    echo $partition_size

    [ ! -e "$mount_point" ] && mkdir "$mount_point"

    get_disk_offsets "$disk_name" | while read offset; do
        echo "Copying data to partition $pnum/4 ..."
        echo "  offset: $offset bytes"

        loop_device=$(losetup -f)

        losetup -o $offset --sizelimit "$partition_size" \
                "$loop_device" "$disk_name"
        if [ $? -gt 0 ]; then
            echo "ERROR: Could not create a loop device: $loop_device"
            exit 1
        fi

        mount "$loop_device" "$mount_point"
        if [ $? -gt 0 ]; then
            echo "ERROR: Could not mount a loop device '$loop_device' to '$mount_point'"
            exit 1
        fi

        source_dir=$(ls -1 "$partitions_dir" | sed -n "${pnum}p")
        cp -a "${partitions_dir}/${source_dir}/." "$mount_point/"
        umount "$mount_point"
        losetup -d "$loop_device"
        echo "Copying data to partition $pnum/4 ... done"
        : $((pnum++))
    done
}

###

print_make_help_and_exit() {
    echo "\
grapemaker make [options] <disk-name>

Options:
  --size, -s <size>         Disk size (in megabytes)
  --format, -f <partitions> Format partitions.  The argument should be a list
                            of partition numbers, like this: 1,2,3,4
  --help, -h                Print this message and exit.
"
    exit 0
}

# Synopsis:
#   main disk_name [options] disk_name
#
# Options:
#   --size, -s <size>         Disk size (in megabytes)
#   --format, -f <partitions> Format partitions.  The argument should be a list
#                             of partition numbers, like this: 1,2,3,4
#   --help, -h                Print help message and exit.
handle_make() {
    local disk_name
    local disk_size
    local format_plist          # Partitions list
    # Parse CLI options
    local temp=$(getopt -o h,s:,f: --long help,size:,format: -- "$@")
    eval set -- "$temp"

    while true; do
        case "$1" in
            --help | -h)
                print_make_help_and_exit
                shift
                ;;
            --size | -s)
                disk_size="$2"
                shift 2
                ;;
            --format | -f)
                local IFS=","
                format_plist=($2)
                unset IFS
                shift 2
                ;;
            *)
                break
                ;;
        esac
    done
    disk_name="$2"

    echo "disk_name:" $disk_name
    echo "size:" $disk_size
    echo "format_plist:" ${format_plist[@]}

    if [ -z "$disk_name" ]; then
        echo "ERROR: Disk name not provided."
        print_help_and_exit
    fi

    mk_empty_disk "$disk_name" "$disk_size"

    disk_init "$disk_name" "$disk_size"
    disk_format_partitions "$disk_name" ${format_plist[@]}
}

###

print_cp_help_and_exit() {
    echo "\
Copy data from SOURCE to the specified DESTINATION.

Synopsis:
  handle_cp [options] source destination

Destination should be in the following format:
  <disk-name>:/<partition-number>/

Example:
  handle_cp ./disk/1/ grape:/1/

Options:
  --help, -h            Print help message and exit.
  --encrypted, -e       Set this option if a target partition was encrypted
                        with veracrypt.
  --encrypted-password=<password>
                        Specify the password for an encrypted partition.
  --encrypted-pim=<pim> Specify the PIM for an encrypted partiiton.
"
    exit 0
}

# Copy data from SOURCE to the specified DESTINATION.
#
# Synopsis:
#   handle_cp [options] source destination
#
# Destination should be in the following format:
#   <disk-name>:/<partition-number>/
#
# Example:
#   handle_cp ./disk/1/ grape:/1/
#
# Options:
#   --help, -h        Print help message and exit.
#   --encrypted, -e   Set this option if a target partition was encrypted
#                     with veracrypt.
#   --encrypted-password=<password>
#                     Specify the password for an encrypted partition.
#   --encrypted-pim=<pim>
#                     Specify the PIM for an encrypted partiiton.
handle_cp() {
    local src
    local dst
    local disk_name
    local partition_number
    local mount_point
    local is_encrypted=false
    local loop_device
    local encrypted_password encrypted_pim

    local temp=$(getopt -o h,e \
                        --long help,encrypted,encrypted-password:,encrypted-pim: \
                        -- "$@")
    eval set -- "$temp"
    while true; do
        case "$1" in
            --help | -h)
                print_cp_help_and_exit
                ;;
            --encrypted | -e)
                is_encrypted=true
                shift 1
                ;;
            --encrypted-password)
                encrypted_password="$2"
                shift 2
                ;;
            --encrypted-pim)
                encrypted_pim="$2"
                shift 2
                ;;
            *)
                break
                ;;
        esac
    done
    src="$2"
    dst="$3"
    echo "Copying data from '$src' to '$dst' ..."

    disk_name=$(echo "$dst" | sed -re "s/(^.*):.*/\1/g")
    partition_number=$(echo "$dst" | sed -re "s/.*:\/([0-9]+)\/.*/\1/g")
    echo "  disk name: $disk_name, partition number: $partition_number"

    mount_point=$(mktemp -d)
    if [ $? -gt 0 ]; then
        echo "ERROR: Could not create a temporary directory: $mount_point"
        exit 1
    fi
    echo "  mount point: $mount_point"

    if [ "$is_encrypted" = "false" ]; then
        loop_device=$(mount_partition "$disk_name" "$partition_number" \
                                      "$mount_point")
    else
        loop_device=$(mount_encrypted_partition \
                          --password="$encrypted_password" \
                          --pim="$encrypted_pim" \
                          "$disk_name" \
                          "$partition_number" \
                          "$mount_point")
    fi

    echo "  copying data"
    cp -r "$src" "$mount_point"
    sync
    echo "  unmounting '$mount_point'"

    if [ "$is_encrypted" = "false" ]; then
        umount "$mount_point"
        losetup -d "$loop_device"
    else
        dismount_encrypted_partition "$mount_point"
    fi

    rm -d "$mount_point"

    echo "Copying data from '$src' to '$dst' ... done"
}

###

print_rm_help_and_exit() {
    echo "\
Remove a specified partition.

Synopsis:
  grapemaker rm <disk-name> <parition-number>

Example:
  grapemaker rm grape1 2
"
    exit 0
}

handle_rm() {
    local disk_name
    local partition_number
    local temp=$(getopt -o h  --long help -- "$@")
    eval set -- "$temp"
    while true; do
        case "$1" in
            --help | -h)
                print_rm_help_and_exit
                ;;
            *)
                break
                ;;
        esac
    done
    disk_name="$2"
    partition_number="$3"

    rm_partition "$disk_name" "$partition_number"
}

###

print_encrypt_help_and_exit() {
    echo "
    Usage: grapemaker encrypt [options] disk-name

    Options:
      --partition, -p           Partition number (1 by default)
      --encryption, -e          Encryption algorigthm ('AES' by default)
      --password, -P            Password ('1' by default)
"
    exit 0
}

# Synopsis:
#   handle_encrypt [options] disk_name
handle_encrypt() {
    local disk_name
    local partition_number="1"
    local encryption_algorithm="AES"

    local loop_device=$(losetup -f)
    local partition_sectors partition_size partition_offset_bytes
    local password="1"

    local temp=$(getopt -o h,p:,e:,P: --long help,password:,partition:,encryption: -- "$@")
    eval set -- "$temp"

    while true; do
        case "$1" in
            --help | -h)
                print_encrypt_help_and_exit
                shift
                ;;
            --partition | -p)
                partition_number="$2"
                shift 2
                ;;
            --encryption | -e)
                encryption_algorithm="$2"
                shift 2
                ;;
            --password | -P)
                password="$2"
                shift 2
                ;;
            *)
                break
                ;;
        esac
    done
    disk_name="$2"

    loop_device=$(make_loop_device "$disk_name" "$partition_number")

    veracrypt \
        --text \
        --non-interactive \
        --create "$loop_device" \
        --volume-type=normal \
        --encryption="$encryption_algorithm" \
        --pim=1 \
        --random-source=/dev/urandom \
        --password="$password" \
        --keyfiles="" \
        --hash=SHA-512 \
        --filesystem=ext4
}

###

# Just print a help message and exit.   :-)
print_help_and_exit() {
    echo "
Usage: grapemaker <action>

Available actions:
  make
  encrypt
  cp
  rm

See '--help' (or '-h') for each action for detailed help.
"

    exit 1
}

### Entry point.

main() {
    set -x
    case "$1" in
        --help | -h)
            print_help_and_exit
            ;;
        make | m)
            shift 1
            handle_make $*
            exit 0
            ;;
        encrypt | e)
            shift 1
            handle_encrypt $*
            exit 0
            ;;
        cp)
            shift 1
            handle_cp $*
            exit 0
            ;;
        rm)
            shift 1
            handle_rm $*
            exit 0
            ;;
    esac

    print_help_and_exit
}

main $*

### make-disk.sh ends here.
